import {describe, expect, test} from '@jest/globals';
import renderer from 'react-test-renderer';
import GenreOptions from '../GenreOptions';
import React from 'react';

describe('Genre Option Component', () => {
  test('renders genre option component correctly', () => {
    const tree = renderer
      .create(
        <GenreOptions
          Options={[]}
          activeGenre={''}
          onPress={function (): void {
            throw new Error('Function not implemented.');
          }}
        />,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
