import React from 'react';
import renderer from 'react-test-renderer';
import {render} from '@testing-library/react-native';
import {expect, it, test} from '@jest/globals';
import Card from '../Card';

test('renders Card Component correctly', () => {
  const tree = renderer
    .create(<Card title={''} edition_count={''} authors={undefined} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('should render the loan book button', () => {
  const {getByTestId} = render(
    <Card title={''} edition_count={''} authors={undefined} />,
  );
  const loanButton = getByTestId('loanBook');
  expect(loanButton).toBeTruthy();
});
