/* eslint-disable react-native/no-inline-styles */
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';
import {responsiveSize} from '../../utils/ResponsiveSize';

interface IOptionButton {
  option: string;
  isActive: boolean;
  key?: any;
  onPress?: (option?: string) => void;
  disabled?: boolean;
  testID?: string;
}

const OptionButton = ({
  isActive,
  onPress,
  key,
  option,
  disabled,
  testID,
}: IOptionButton) => {
  return (
    <TouchableOpacity
      testID={testID}
      disabled={disabled}
      style={[
        styles.genreBtn,
        {
          borderColor: isActive ? '#40BFFF' : '#EBF0FF',
          backgroundColor: isActive ? '#40BFFF' : 'white',
        },
      ]}
      key={key}
      onPress={() => onPress && onPress(option)}>
      <Text style={[styles.genreText, {color: isActive ? 'white' : '#223263'}]}>
        {option}
      </Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  genreBtn: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: '#EBF0FF',
    backgroundColor: 'white',
  },
  genreText: {
    paddingHorizontal: 8,
    paddingVertical: 4,
  },
  optionGenre: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: responsiveSize(16),
    gap: 16,
  },
});

export default OptionButton;
