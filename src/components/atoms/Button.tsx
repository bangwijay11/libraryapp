import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';

interface IButton {
  title?: string;
  testID?: string;
  onPress?: () => void;
  style?: any;
}
const Button = ({onPress, title, style, testID}: IButton) => {
  return (
    <TouchableOpacity
      testID={testID}
      style={[styles.bntContainer, style]}
      onPress={onPress}>
      <Text style={styles.btnText}>{title}</Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  bntContainer: {
    position: 'absolute',
    backgroundColor: '#40BFFF',
    borderRadius: 5,
    bottom: 8,
    right: 8,
  },
  btnText: {color: 'white', paddingHorizontal: 8, paddingVertical: 4},
});
export default Button;
