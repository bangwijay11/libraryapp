import {describe, expect, it, test} from '@jest/globals';
import renderer from 'react-test-renderer';
import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import OptionButton from '../OptionButton';

describe('Option Button Component', () => {
  test('renders  option button component correctly', () => {
    const tree = renderer
      .create(<OptionButton option={''} isActive={false} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should set isActive to true when an Option button is clicked', () => {
    const {getByTestId} = render(
      <OptionButton testID="testButton" option="Test" isActive={false} />,
    );
    const button = getByTestId('testButton');

    const style = button.props.style;
    fireEvent.press(button);
    expect(style.backgroundColor).toBe('#40BFFF');
  });
});
