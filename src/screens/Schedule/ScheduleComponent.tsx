import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
import DatePicker from 'react-native-date-picker';
import {responsiveSize} from '../../utils/ResponsiveSize';

interface ISchedule {
  bookData: any;
  date: any;
  time: any;
  openDate: boolean;
  openTime: boolean;
  handleSelectTime: (value: any) => void;
  handleSelectDate: (value: any) => void;
  handleOpenModalDate: (open: any) => void;
  handleOpenModalTime: (open: any) => void;
  onSubmit: () => void;
}
const ScheduleComponent = ({
  bookData,
  date,
  time,
  openDate,
  openTime,
  handleSelectDate,
  handleSelectTime,
  handleOpenModalTime,
  handleOpenModalDate,
  onSubmit,
}: ISchedule) => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Make An Appointment</Text>
      <Text style={styles.title}>Book Information</Text>

      <View style={{marginBottom: responsiveSize(16)}}>
        <Text>Title : {bookData.title}</Text>
        <Text style={{marginVertical: responsiveSize(8)}}>
          Edition Number : {bookData.edition_count}
        </Text>
        <Text>Authors : </Text>
        {bookData.authors.map((author: any) => {
          return <Text key={author.key}>- {author.name}</Text>;
        })}
      </View>
      <Text style={styles.title}>Choose a time to pick up the book</Text>
      <View style={styles.selectDateContainer}>
        <View style={{width: '35%'}}>
          <Text>Date</Text>
          <TouchableOpacity
            style={styles.date}
            onPress={() => handleOpenModalDate(true)}>
            <Text>{date.toLocaleDateString()}</Text>
          </TouchableOpacity>
        </View>
        <View style={{width: '30%'}}>
          <Text>Time</Text>
          <TouchableOpacity
            style={styles.date}
            onPress={() => handleOpenModalTime(true)}>
            <Text>{`${time.getHours()}:${time.getMinutes()} `}</Text>
          </TouchableOpacity>
        </View>
      </View>
      <DatePicker
        modal
        mode="date"
        open={openDate}
        date={date}
        onConfirm={value => {
          handleSelectDate(value);
        }}
        onCancel={() => {
          handleOpenModalDate(false);
        }}
      />
      <DatePicker
        modal
        mode="time"
        open={openTime}
        date={date}
        onConfirm={value => {
          handleSelectTime(value);
        }}
        onCancel={() => {
          handleOpenModalTime(false);
        }}
      />

      <TouchableOpacity style={styles.submitBtn} onPress={onSubmit}>
        <Text style={styles.submitTxt}>Submit</Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  header: {
    textAlign: 'center',
    fontWeight: '700',
    marginBottom: responsiveSize(32),
    fontSize: 22,
  },
  container: {padding: responsiveSize(16), backgroundColor: 'white', flex: 1},
  submitBtn: {
    width: '100%',
    borderRadius: 8,
    backgroundColor: '#40BFFF',
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    marginTop: responsiveSize(32),
  },
  submitTxt: {color: 'white', fontWeight: '600', fontSize: 16},
  selectDateContainer: {
    flexDirection: 'row',
    width: '100%',
    gap: 30,
  },
  date: {
    borderWidth: 1,
    borderRadius: 10,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: responsiveSize(8),
  },
  title: {fontWeight: '600', marginBottom: responsiveSize(16)},
});

export default ScheduleComponent;
