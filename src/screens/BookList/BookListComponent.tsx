/* eslint-disable react-native/no-inline-styles */
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import React from 'react';
import {responsiveSize} from '../../utils/ResponsiveSize';
import {Card} from '../../components/molecules';
import GenreOptions from '../../components/molecules/GenreOptions';
import {OptionButton} from '../../components/atoms';

interface IBookList {
  data: any;
  genreOption: string[];
  getBookByGenre: (genre: string) => void;
  handleLoan: (bookData: object) => void;
  activeGenre: string;
  isLoading: boolean;
  next: () => void;
  prev: () => void;
  activePage: number;
}

type Author = {
  name: string;
  key: string;
};

type ItemData = {
  key: string;
  title: string;
  authors: Author[];
  edition_count: number;
};

type ItemProps = {
  item: ItemData;
  onPress: () => void;
  testID: string;
};

const Item = ({item, onPress, testID}: ItemProps) => {
  const {title, authors, edition_count} = item;
  return (
    <Card
      title={title}
      authors={authors}
      edition_count={edition_count}
      onPress={onPress}
      testID={testID}
    />
  );
};

const BookListComponent = ({
  data,
  genreOption,
  getBookByGenre,
  activeGenre,
  isLoading,
  prev,
  next,
  activePage,
  handleLoan,
}: IBookList) => {
  const renderItem = ({item, index}: {item: ItemData; index: any}) => {
    return (
      <Item
        testID={`loanBook-${index + 1}`}
        item={item}
        onPress={() => handleLoan(item)}
      />
    );
  };
  return (
    <View style={styles.container}>
      <>
        <View style={styles.optionGenre}>
          <GenreOptions
            Options={genreOption}
            onPress={getBookByGenre}
            activeGenre={activeGenre}
          />
        </View>
        {isLoading === true ? (
          <ActivityIndicator />
        ) : (
          <>
            <FlatList
              data={data}
              renderItem={renderItem}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={styles.list}
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
                gap: 8,
              }}>
              <OptionButton
                testID="prevButton"
                option="Prev"
                onPress={prev}
                isActive
                disabled={activePage <= 1}
              />
              <Text>Page {activePage}</Text>
              <OptionButton
                testID="nextButton"
                option="Next"
                onPress={next}
                isActive
              />
            </View>
          </>
        )}
      </>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, padding: responsiveSize(16), backgroundColor: 'white'},
  optionGenre: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: responsiveSize(16),
    gap: 16,
  },
  list: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    gap: responsiveSize(10),
  },
});

export default BookListComponent;
