import React, {useEffect, useState} from 'react';
import BookListComponent from './BookListComponent';
import axios from 'axios';

const BookListContainer = ({navigation}: any) => {
  const [bookList, setBookList] = useState([]);
  const [activeGenre, setActiveGenre] = useState('Sport');
  const genreOption = ['Love', 'Sport', 'Market', 'Football'];
  const [isLoading, setIsLoading] = useState(false);
  const [activePage, setActivePage] = useState(0);
  const data = bookList.slice(
    activePage * 4,
    activePage < 1 ? 4 : activePage * 4 + 4,
  );
  const next = () => setActivePage(activePage + 1);
  const prev = () => setActivePage(activePage - 1);

  const getBookByGenre = (genre: string) => {
    setActiveGenre(genre);
    setActivePage(0);
  };
  const getBookApiByGenre = (genre: string) => {
    setIsLoading(true);
    axios
      .get(`https://openlibrary.org/subjects/${genre}.json?details=true`)
      .then(resp => {
        setBookList(resp.data?.works);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };
  useEffect(() => {
    getBookApiByGenre(activeGenre);
  }, [activeGenre]);

  const handleLoan = (bookData: object) => {
    navigation.navigate('Schedule', {bookData});
  };

  return (
    <BookListComponent
      data={data}
      genreOption={genreOption}
      getBookByGenre={getBookByGenre}
      activeGenre={activeGenre}
      isLoading={isLoading}
      next={next}
      prev={prev}
      activePage={activePage + 1}
      handleLoan={handleLoan}
    />
  );
};

export default BookListContainer;
