import React from 'react';
import renderer from 'react-test-renderer';
import BookList from '../BookListComponent';
import {expect, it, jest, test} from '@jest/globals';
import BookListComponent from '../BookListComponent';
import {fireEvent, render} from '@testing-library/react-native';

test('renders BookListComponent correctly', () => {
  const tree = renderer
    .create(
      <BookList
        data={undefined}
        genreOption={[]}
        getBookByGenre={function (): void {
          throw new Error('Function not implemented.');
        }}
        handleLoan={function (): void {
          throw new Error('Function not implemented.');
        }}
        activeGenre={''}
        isLoading={false}
        next={function (): void {
          throw new Error('Function not implemented.');
        }}
        prev={function (): void {
          throw new Error('Function not implemented.');
        }}
        activePage={0}
      />,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('should call the onPress function when button loan pressed', () => {
  const data = [
    {
      title: 'Nice Food',
      authors: [
        {
          key: '1',
          name: 'ali',
        },
      ],
      edition_count: 321,
    },
    {
      title: 'Nice Drink',
      authors: [
        {
          key: '1',
          name: 'anton',
        },
      ],
      edition_count: 301,
    },
  ];
  const onItemPress = jest.fn();
  const {getByTestId} = render(
    <BookListComponent
      data={data}
      handleLoan={onItemPress}
      genreOption={[]}
      getBookByGenre={function (): void {
        throw new Error('Function not implemented.');
      }}
      activeGenre={''}
      isLoading={false}
      next={function (): void {
        throw new Error('Function not implemented.');
      }}
      prev={function (): void {
        throw new Error('Function not implemented.');
      }}
      activePage={0}
    />,
  );
  const loanButton1 = getByTestId('loanBook-1');
  fireEvent.press(loanButton1);
  expect(onItemPress).toHaveBeenCalledWith(data[0]);
});
